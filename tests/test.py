#!/usr/bin/env python

import os
import unittest
from atooms.models import models, samples, schema

root = os.path.dirname(__file__)

class Test(unittest.TestCase):

    def setUp(self):
        pass

    def _compare_data(self, ref, copy):
        with open(ref) as fh:
            data = fh.read()
        with open(copy) as fh:
            data_copy = fh.read()
        self.assertEqual(data, data_copy)

    def test_grammar(self):
        models.all()
        models.get("lennard_jones")
        samples.all()
        samples.get("lennard_jones-5cc3b80bc415fa5c262e83410ca65779.xyz")

    def test(self):
        sample = samples.get("lennard_jones-5cc3b80bc415fa5c262e83410ca65779.xyz")
        raw = root + '/../atooms/models/storage/lennard_jones-5cc3b80bc415fa5c262e83410ca65779.xyz'
        self._compare_data(raw, sample)

    def test_convert(self):
        m = {
            "metadata": {'name': 'lj'},
            "potential": [
                {
                    "type": "lennard_jones",
                    "parameters": {"sigma": [[1.0, 0.8], [0.8, 0.88]],
                                   "epsilon": [[1.0, 1.5], [1.5, 0.5]]}
                }
            ],
            "cutoff": [
                {
                    "type": "cut_shift",
                    "parameters": {"rcut": [[2.5, 2.0], [2.0, 2.2]]}
                }
            ]
        }
        n = {'potential': [{'cutoff': {'parameters': {'1-1': {'rcut': 2.5},
                                                      '1-2': {'rcut': 2.0},
                                                      '2-2': {'rcut': 2.2}},
                                       'type': 'cut_shift'},
                            'parameters': {'1-1': {'epsilon': 1.0, 'sigma': 1.0},
                                           '1-2': {'epsilon': 1.5, 'sigma': 0.8},
                                           '2-2': {'epsilon': 0.5, 'sigma': 0.88}},
                            'type': 'lennard_jones'}],
             "metadata": {'name': 'lj'},
             }
        self.assertEqual(m, schema._convert(m, 1))
        self.assertEqual(n, schema._convert(m, 2))

    def test_schemas(self):
        from jsonschema import validate
        from atooms.models import models

        for version in [1, ]:
            models.default_table_name = version
            for model in models:
                # Moving the payload to _model means that we cannot
                # use model right away, we must get it
                validate(instance=models.get(model['name']),
                         schema=schema.schemas[version])

    def test_storage(self):
        import glob
        from atooms.models import Query
        query = Query()
        sample = samples.search((query.model == 'lennard_jones') &
                                (query.density == 1.0))[0]
        name = sample['name']
        self.assertTrue(len(samples.get(name)) > 0)

    def test_pprint(self):
        with open('/dev/null', 'w') as null:
            models.pprint(file=null)
            models.pprint(include=['name', 'doi'], file=null)

    def test_potentials(self):
        from atooms.models import potentials
        for f in potentials.values():
            f(0.9)

    def tearDown(self):
        pass


# class _Test(unittest.TestCase):

#     def setUp(self):
#         pass

#     def _compare_data(self, ref, copy):
#         with open(ref) as fh:
#             data = fh.read()
#         with open(copy) as fh:
#             data_copy = fh.read()
#         self.assertEqual(data, data_copy)

#     def test_grammar(self):
#         from atooms import models

#         # Plain
#         models.available()
#         models.get("lennard_jones")
#         models.samples()
#         models.copy("lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz")

#         # Techy
#         models.models()
#         models.model("lennard_jones")
#         models.samples()
#         models.sample("lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz")

#     def test(self):
#         sample = models.sample("lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz")
#         raw = root + '/../atooms/models/storage/lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz'
#         self._compare_data(raw, sample)

#     def test_convert(self):
#         m = {
#             "metadata": {'name': 'lj'},
#             "potential": [
#                 {
#                     "type": "lennard_jones",
#                     "parameters": {"sigma": [[1.0, 0.8], [0.8, 0.88]],
#                                    "epsilon": [[1.0, 1.5], [1.5, 0.5]]}
#                 }
#             ],
#             "cutoff": [
#                 {
#                     "type": "cut_shift",
#                     "parameters": {"rcut": [[2.5, 2.0], [2.0, 2.2]]}
#                 }
#             ]
#         }
#         n = {'potential': [{'cutoff': {'parameters': {'1-1': {'rcut': 2.5},
#                                                       '1-2': {'rcut': 2.0},
#                                                       '2-2': {'rcut': 2.2}},
#                                        'type': 'cut_shift'},
#                             'parameters': {'1-1': {'epsilon': 1.0, 'sigma': 1.0},
#                                            '1-2': {'epsilon': 1.5, 'sigma': 0.8},
#                                            '2-2': {'epsilon': 0.5, 'sigma': 0.88}},
#                             'type': 'lennard_jones'}],
#              "metadata": {'name': 'lj'},
#              }
#         self.assertEqual(m, models.schema._convert(m, 1))
#         self.assertEqual(n, models.schema._convert(m, 2))

#     def test_schemas(self):
#         from jsonschema import validate
#         from atooms import models

#         for version in [1, 2]:
#             for model in models.models(version):
#                 sv = model["schema_version"]
#                 validate(instance=model, schema=models.schema.schemas[sv])

#     def test_tinydb_compat(self):
#         from tinydb import TinyDB, Query
#         from tinydb.storages import MemoryStorage
#         from atooms import models

#         db = TinyDB(storage=MemoryStorage)
#         db.default_table_name = 1
#         for version in [1, 2]:
#             table = db.table(version)
#             table.insert_multiple(models.models(version))
#         self.assertTrue(len(db.table(1)) > 0)
#         self.assertTrue(len(db.table(2)) > 0)

#     def test_storage(self):
#         import atooms.models
#         import glob
#         try:
#             query = atooms.models.Query()
#         except ModuleNotFoundError:
#             self.skipTest('tinydb not installed')
#         db = atooms.models.samples()
#         samples = db.search((query.model == 'lennard_jones') &
#                             (query.density == 1.0))
#         self.assertTrue(len(atooms.models.copy(samples[0])) > 0)

#     def test_pprint(self):
#         from atooms.models import models
#         db = models()
#         with open('/dev/null', 'w') as null:
#             db.pprint(file=null)
#             db.pprint(include=['name', 'doi'], file=null)

#     def test_potentials(self):
#         from atooms.models import potentials
#         for f in potentials.values():
#             f(0.9)

#     def tearDown(self):
#         pass

if __name__ == '__main__':
    unittest.main()
