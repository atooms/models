import os
import json
from atooms.backends.f90 import Interaction as _Interaction


class Interaction(_Interaction):

    def __init__(self, model, neighbor_list=None,
                 interaction='interaction.f90', helpers='helpers.f90',
                 inline=True, inline_safe=False, debug=False,
                 parallel=False):

        from atooms.models import models
        if not hasattr(model, 'get'):
            if os.path.isfile(model) and model.endswith('json'):
                # This is a json file, we read it
                with open(model) as fh:
                    model = json.load(fh)
            else:
                # This may be a string, so we look for the model in the
                # atooms-database database and replace the string with the dictionary
                model = models.get(model)

        super(). __init__(model, neighbor_list, interaction, helpers,
                          inline=inline, inline_safe=inline_safe,
                          debug=debug, parallel=parallel)
