Atooms models
=============

[![pypi](https://img.shields.io/pypi/v/atooms-models.svg)](https://pypi.python.org/pypi/atooms-models/)
[![version](https://img.shields.io/pypi/pyversions/atooms-models.svg)](https://pypi.python.org/pypi/atooms-models/)
[![license](https://img.shields.io/pypi/l/atooms-pp.svg)](https://en.wikipedia.org/wiki/GNU_General_Public_License)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fatooms%2Fmodels/HEAD?labpath=docs%2Findex.ipynb)
[![pipeline](https://framagit.org/atooms/models/badges/master/pipeline.svg)](https://framagit.org/atooms/models/badges/master/pipeline.svg)
[![coverage report](https://framagit.org/atooms/models/badges/master/coverage.svg?job=test:f90)](https://framagit.org/atooms/models/-/commits/master)

A database of interaction models for classical molecular dynamics and Monte Carlo simulations.

Quick start
-----------

Print all the available models
```python
from atooms import models
for model in models.available():
    print(model["name"])
```
```
gaussian_core
coslovich_pastore
bernu_hiwatari_hansen_pastore
lennard_jones
kob_andersen
roux_barrat_hansen
grigera_cavagna_giardina_parisi
harmonic_spheres
kob_andersen_2
dellavalle_gazzillo_frattini_pastore
wahnstrom
kob_andersen
coslovich_pastore
```

Print all the available samples
```python
for sample in models.samples():
    print(sample["path"])
```
```
lennard_jones-0-13ce47602b259f7802e89e23ffd57f19.xyz
grigera_cavagna_giardina_parisi-0-0ac97fa8c69c320e48bd1fca80855e8a.xyz
coslovich_pastore-0-488db481cdac35e599922a26129c3e35.xyz
lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz
kob_andersen-0-8f4a9fe755e5c1966c10b50c9a53e6bf.xyz
bernu_hiwatari_hansen_pastore-0-f61d7e58b9656cf9640f6e5754441930.xyz
```

Get a local copy of a Lennard-Jones fluid sample
```python
local_file = models.copy("lennard_jones-0-5cc3b80bc415fa5c262e83410ca65779.xyz")
```

The `local_file` can then be used to start a simulation or further analysis using `atooms` packages.

Documentation
-------------
Check out the [documentation](https://atooms.frama.io/docs/models.html) for full details.

Installation
------------
Clone the code repository and install from source
```
git clone https://framagit.org/atooms/models.git
cd sample
make install
```

Install `atooms-models` with pip
```
pip install atooms-models
```

Contributing
------------
Contributions to the project are welcome. If you wish to contribute, check out [these guidelines](https://framagit.org/atooms/atooms/-/blob/master/CONTRIBUTING.md).

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
